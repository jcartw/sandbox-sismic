from sismic.io import import_from_yaml
from sismic.interpreter import Interpreter
from sismic.model import MacroStep
from sismic.helpers import log_trace
from sismic.interpreter import InternalEvent


# Load statechart from yaml file
elevator = import_from_yaml(filepath='./elevator.yaml')

# Create an interpreter for this statechart
interpreter = Interpreter(elevator)

print('Before:', interpreter.configuration)

# put the statechart in its initial configuration
step = interpreter.execute_once()

print('After:', interpreter.configuration)
print("")

print("Create log_trace")
trace = log_trace(interpreter)
print("")

print("Bind a listener")


def my_listener(event):
    print("RECEIVED EVENT START")
    print(event)
    if hasattr(event, "payload"):
        print(getattr(event, "payload"))
    print("RECEIVED EVENT END")


listener = my_listener
interpreter.bind(listener)
print("")
# interpreter._raise_event(InternalEvent('test'))

print("Queue floorSelected event, then run execute")
interpreter.queue('floorSelected', floor=1)
# interpreter.execute_once()
interpreter.execute()
print("")


#print("See log_trace")
# for k in range(0, len(trace)):
#    print(f"{k}: ")
#    # print(trace[k])
#    for attribute in ['event', 'transitions', 'exited_states', 'entered_states', 'sent_events']:
#        print('{}: {}'.format(attribute, getattr(trace[k], attribute)))
