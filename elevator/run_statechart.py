from sismic.io import import_from_yaml
from sismic.interpreter import Interpreter
from sismic.model import MacroStep
from sismic.helpers import log_trace

# Load statechart from yaml file
elevator = import_from_yaml(filepath='./elevator.yaml')

# Create an interpreter for this statechart
interpreter = Interpreter(elevator)

print('Before:', interpreter.configuration)

# put the statechart in its initial configuration
step = interpreter.execute_once()

print('After:', interpreter.configuration)
print("")

print("Get attributes from 'step'")
for attribute in ['event', 'transitions', 'entered_states', 'exited_states', 'sent_events']:
    print('{}: {}'.format(attribute, getattr(step, attribute)))
print("")

print("Queue events")
interpreter.queue('click')
interpreter.execute_once()  # Process the "click" event

interpreter.queue('clack')  # An event name can be provided as well
interpreter.execute_once()  # Process the "clack" event

interpreter.queue('click', 'clack')
interpreter.execute_once()  # Process "click"
step = interpreter.execute_once()  # Process "clack"
for attribute in ['event', 'transitions', 'entered_states', 'exited_states', 'sent_events']:
    print('{}: {}'.format(attribute, getattr(step, attribute)))
print("")

print("Queue returns the interpreter, so it can be chained")
interpreter.queue('click', 'clack', 'clock').execute_once()
for time, event in interpreter._external_queue:
    print(event.name)
print("")

print("To process all remaining events, wait util execute_once() returns None:")
while interpreter.execute_once():
    pass
print("")

print("For convenience, an interpreter has an execute() method that repeatedly call execute_once() and that returns a list of its output")
interpreter.queue('click', 'clack')
for step in interpreter.execute():
    print(step)
    assert isinstance(step, MacroStep)
print("")

print('note that execute() runs all remaining steps before returning a list, so you can specify max_steps"')
interpreter.queue('click', 'clack', 'clock')
assert len(interpreter.execute(max_steps=2)) <= 2
# 'clock' is not yet processed
assert len(interpreter.execute()) == 1
print("")

print("you can see all possible events for this statechart")
print(elevator.events_for(interpreter.configuration))
print("")

print("Create a 'floorSelected' event with floor=1 as a param")
print('Current floor is', interpreter.context['current'])
interpreter.queue('floorSelected', floor=1)
interpreter.execute()
print('Current floor is', interpreter.context['current'])
print("")

print("Test out log_trace")
trace = log_trace(interpreter)
interpreter.queue('floorSelected', floor=4)
interpreter.execute()
for k in range(0, len(trace)):
    print(f"{k}: ")
    # print(trace[k])
    for attribute in ['event', 'transitions', 'exited_states', 'entered_states', 'sent_events']:
        print('{}: {}'.format(attribute, getattr(trace[k], attribute)))
