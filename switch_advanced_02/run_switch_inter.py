import keyboard
import time

from sismic.io import import_from_yaml
from sismic.interpreter import Interpreter
from sismic.model import MacroStep
from sismic.helpers import log_trace
from sismic.interpreter import InternalEvent


# Load statechart from yaml file
statechart = import_from_yaml(filepath='./advanced_switch.yaml')

# Create an interpreter for this statechart
interpreter = Interpreter(statechart)


def queue_flick_event(x):
    print("Flick!")
    interpreter.queue("flick")


def show_stats(x):
    print("Queue: ", interpreter._external_queue)
    print('Config:', interpreter.configuration)
    print("")


keyboard.on_press_key("f", queue_flick_event)
keyboard.on_press_key("s", show_stats)


# event listener
def event_listener(event):
    print("START: RECEIVED EVENT")
    print(event)
    if hasattr(event, "payload"):
        print(getattr(event, "payload"))
    print("END: RECEIVED EVENT")


interpreter.bind(event_listener)


print("Start Statechart Engine")
interpreter.clock.speed = 1
interpreter.clock.start()
while True:
    interpreter.execute_once()
    # print("Clock time: ")
    # print(interpreter.clock.time)
    time.sleep(0.1)
