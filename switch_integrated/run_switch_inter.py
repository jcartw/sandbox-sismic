import keyboard
import time

from sismic.io import import_from_yaml
from sismic.interpreter import Interpreter
from sismic.model import MacroStep
from sismic.helpers import log_trace
from sismic.interpreter import InternalEvent

from switch import Switch

# sw = Switch()

# Load statechart from yaml file
statechart = import_from_yaml(filepath='./switch.yaml')

# Create an interpreter for this statechart
sw = Switch()
interpreter = Interpreter(statechart, initial_context={'switch': sw})

#self.interpreter = Interpreter(statechart, initial_context={'stopwatch': self.stopwatch})


def queue_flick_event(x):
    print("Flick!")
    interpreter.queue("flick")


def show_stats(x):
    print("Queue: ", interpreter._external_queue)
    print('Config:', interpreter.configuration)
    print("")
    sw.print_on_off_state()


keyboard.on_press_key("f", queue_flick_event)
keyboard.on_press_key("s", show_stats)
keyboard.on_press_key("l", lambda _: sw.lock())
keyboard.on_press_key("u", lambda _: sw.unlock())

print("Start Statechart Engine")
interpreter.clock.speed = 1
interpreter.clock.start()
while True:
    interpreter.execute_once()
    # print("Clock time: ")
    # print(interpreter.clock.time)
    time.sleep(0.1)
