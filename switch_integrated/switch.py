
class Switch:
    def __init__(self):
        self.on_off = 0
        self.unlocked = 1

    def turn_on(self):
        print("Turning ON")
        self.on_off = 1

    def turn_off(self):
        print("Turning OFF")
        self.on_off = 0

    def print_on_off_state(self):
        print("Switch is in state: {0}".format(self.on_off))

    def lock(self):
        self.unlocked = 0

    def unlock(self):
        self.unlocked = 1

    def is_unlocked(self):
        ans = self.unlocked == 1
        print("Switch is unlocked: {0}".format(ans))
        return ans


if __name__ == "__main__":
    s = Switch()
    s.turn_on()

    s.lock()
    x = s.is_unlocked()
    print(x)

    s.unlock()
    x = s.is_unlocked()
    print(x)
