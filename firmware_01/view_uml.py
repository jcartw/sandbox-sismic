from sismic.io import import_from_yaml, export_to_plantuml

# Load statechart from yaml file
sc = import_from_yaml(filepath='./iot_device.yaml')
# sc = import_from_yaml(filepath='./notifications.yaml')

plantuml = export_to_plantuml(sc)

print(plantuml)

# http://www.plantuml.com/plantuml/
