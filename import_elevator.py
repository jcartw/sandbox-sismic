from sismic.io import import_from_yaml, export_to_plantuml
from sismic.model import Statechart

with open('./elevator.yaml') as f:
    statechart = import_from_yaml(f)
    assert isinstance(statechart, Statechart)

print(statechart)

print(export_to_plantuml(statechart))


