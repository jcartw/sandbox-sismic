from sismic.io import import_from_yaml
from sismic.interpreter import Interpreter
from sismic.model import MacroStep
from sismic.helpers import log_trace
from sismic.interpreter import InternalEvent


# Load statechart from yaml file
statechart = import_from_yaml(filepath='./switch.yaml')

# Create an interpreter for this statechart
interpreter = Interpreter(statechart)

# show queue and config
print("Queue: ", interpreter._external_queue)
print('Config:', interpreter.configuration)
print("")

# put the statechart in its initial configuration
interpreter.execute_once()
print("Queue: ", interpreter._external_queue)
print('Config:', interpreter.configuration)
print("")

# run another execution step
interpreter.execute_once()
print("Queue: ", interpreter._external_queue)
print('Config:', interpreter.configuration)
print("")

# run another execution step
interpreter.execute_once()
print("Queue: ", interpreter._external_queue)
print('Config:', interpreter.configuration)
print("")

# queue up a 'flick' event
interpreter.queue('flick')
print("Queue: ", interpreter._external_queue)
print('Config:', interpreter.configuration)
print("")

# run another execution step
interpreter.execute_once()
print("Queue: ", interpreter._external_queue)
print('Config:', interpreter.configuration)
print("")

# run another execution step
interpreter.execute_once()
print("Queue: ", interpreter._external_queue)
print('Config:', interpreter.configuration)
print("")

# queue up a two 'flick' events
interpreter.queue('flick', 'flick')
print("Queue: ", interpreter._external_queue)
print('Config:', interpreter.configuration)
print("")

# run another execution step
interpreter.execute_once()
print("Queue: ", interpreter._external_queue)
print('Config:', interpreter.configuration)
print("")

# run another execution step
interpreter.execute_once()
print("Queue: ", interpreter._external_queue)
print('Config:', interpreter.configuration)
print("")
